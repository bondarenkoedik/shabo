url = require 'url'
async = require 'async'
passport = require 'passport'
base64url = require 'b64url'
crypto = require 'crypto'
nodemailer = require 'nodemailer'
smtpTransport = require 'nodemailer-smtp-transport'
md5 = require 'MD5'
pth = require 'path'

Model = require './model'
View = require './view'
socialConfig = require '../meta/socialConfig'
config = require '../meta/config'
locale = require '../locale'

getQueryLang = (url) ->
	myRegexp = /^\/(ru|ua|en)/
	match = myRegexp.exec url
	return match?[1] or 'ru'


params =
	admin:
		failureRedirect: '/admin/login'
		successRedirect: '/admin/pages'
		session: true
	user:
		failureRedirect: '/'
		successRedirect: '/'
		session: true
	facebook:
		failureRedirect: '/'
		successRedirect: '/'
		session: true
	vk: ''
	odnoklassniki:
		scope: [
			'VALUABLE_ACCESS'
		]



exports.isAuth = (req, res, next)->
	path = url.parse req.path

	if path.path == '/login'
		return next()

	if req.user and req.user?.role isnt 'admin'
		return res.redirect '/'

	if not req.user or not req.isAuthenticated() or req.user?.role isnt 'admin'
		return res.redirect '/admin/login'

	next()



authenticate = (strategy) ->
	passport.authenticate strategy, params[strategy]

exports.authenticate = authenticate



exports.register = (req, res) ->
	data = req.query
	registeredUser = {}

	lang = getQueryLang req.originalUrl
	loc = locale[lang]

	if data.email && data.password
		async.waterfall [
			(next) ->
				Model 'User', 'findOne', next, {email: data.email}
			(doc, next) ->
				if doc
					View.clientSuccess {err: {code: 409, message: 'Пользователь с таким почтовым адресом уже зарегистрирован'}}, res
				else
					data.username = data.email
					data.role = 'user'

					Model 'User', 'create', next, data
			(doc, next) ->
				registeredUser = doc
				sendRegistrationConfirmation doc, loc, next
			(info, next) ->
				View.clientSuccess {user: registeredUser, info: info}, res

		], (err) ->
			View.clientFail err, res
	else
		View.clientError {code: 400, message: 'Not enough data'}, res



exports.userLogin = (req, res) ->
	passport.authenticate("user", (err, user, info) ->
		return View.clientFail err, res if err
		return View.clientError {code: 401, message: 'Неверный ввод данных'}, res unless user
		req.logIn user, (err) ->
			return View.clientFail err, res if err
			View.clientSuccess user, res

		return
	) req, res


exports.userLogout = (req, res) ->
	req.logout()
	View.clientSuccess true, res


exports.facebook = (req, res) ->
	data = req.body.response
	facebookUser = null

	async.waterfall [
		(next) ->
			if data and data.authResponse and data.authResponse.signedRequest
				parseSignedRequestFacebook data.authResponse.signedRequest, socialConfig.facebook.clientSecret, next
			else
				View.clientError {code: 400, message: 'Not enough data'}, res
		(result, next) ->
			facebookUser = data.user
			Model 'User', 'findOne', next, {'facebook.id': result.user_id}
		(doc, next) ->
			if doc
				facebookLogin req, res, doc
			else
				data = {
					username: facebookUser.name,
					role: 'user',
					password: facebookUser.id,
					firstName: facebookUser.first_name,
					lastName:facebookUser.last_name,
					facebook: facebookUser,
					email: "#{facebookUser.id}@facebook.com"
					image: facebookUser.image
				}

				Model 'User', 'create', next, data
		(doc, next) ->
			facebookLogin req, res, doc

	], (err) ->
		View.clientFail err, res



facebookLogin = (req, res, user) ->

	req.body.username = user.username
	req.body.password = user.password

	passport.authenticate("facebookLocal", (err, user, info) ->

		return View.clientFail err, res if err
		unless user
			if info?.message
				View.clientFail {code: 400, message: info.message}, res
			else
				View.clientFail {code: 401, message: 'User not found'}, res unless user

		req.logIn user, (err) ->
			return View.clientError err, res if err
			View.clientSuccess user, res

	) req, res



parseSignedRequestFacebook = (signedRequest, secret, cb) ->
	encoded_data = signedRequest.split(".", 2)

	sig = encoded_data[0]
	json = base64url.decode(encoded_data[1])
	data = JSON.parse(json)

	if not data.algorithm or data.algorithm.toUpperCase() isnt "HMAC-SHA256"
		cb {code: 400, message: "Unknown algorithm. Expected HMAC-SHA256"}, null

	expected_sig = crypto.createHmac("sha256", secret).update(encoded_data[1]).digest("base64").replace(/\+/g, "-").replace(/\//g, "_").replace("=", "")
	if sig isnt expected_sig
		cb {code: 403, message: "Incorrect hash key"}, null

	cb null, data



exports.vk = (req, res) ->
	data = req.body.response?.session

	View.clientError {code: 400, message: 'Not enough data'}, res unless data

	async.waterfall [
		(next) ->
			parseSignedRequestVk data, next
		(result, next) ->
			return View.clientError {code: 418, message: 'Cool hacker'}, res unless result

			Model 'User', 'findOne', next, {'vk.id': data.user.id}
		(doc, next) ->
			if doc
				vkLogin req, res, doc
			else
				data = {
					username: data.user.id,
					role: 'user',
					password: data.user.id,
					firstName: data.user.first_name,
					lastName: data.user.last_name,
					vk: data.user,
					email: "#{data.user.id}@vk.com"
					image: data.user.image
				}

				Model 'User', 'create', next, data
		(doc, next) ->
			vkLogin req, res, doc

	], (err) ->
		View.clientFail err, res



vkLogin = (req, res, user) ->

	req.body.username = user.username
	req.body.password = user.password

	passport.authenticate("vkLocal", (err, user, info) ->

		return View.clientFail err, res if err
		unless user
			if info?.message
				View.clientFail {code: 400, message: info.message}, res
			else
				View.clientFail {code: 401, message: 'User not found'}, res unless user

		req.logIn user, (err) ->
			return View.clientError err, res if err
			View.clientSuccess user, res

	) req, res



sendRegistrationConfirmation = (user, locale, cb) ->
	cb null, {errors: [{responseCode: 400, response: 'No data received'}]} unless user

	transporter = nodemailer.createTransport(smtpTransport(
		host: config.mail.host
		port: config.mail.port
		auth:
			user: config.mail.user
			pass: config.mail.pass))

	date = new Date()
	year = date.getFullYear()

	mailOptions =
		from: config.registrationEmail
		to: "#{user.email}"
		subject: locale.mail.registration.subject
		text: "http://#{config.domain}?userActivation=#{user._id}"
		html: "<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	<meta name='viewport' content='width=device-width, initial-scale=1.0' />
</head>
<body leftmargin='0' marginwidth='10' topmargin='0' marginheight='0' offset='0' width='580' style='max-width:580px;font-family:Arial,sans-serif;'>

<table style='max-width: 570px; background-color: #f1f2f3; color:  #515151;'>

    <tr style='border-bottom: 1px solid rgba(196, 154, 108, 0.3); vertical-align: top;'>
        <td style=' padding: 20px 0;'>
	        <a href='http://shabo.ua'>
		        <img src='http://shabo.ua/img/emailLogo.png' alt=''/>
            </a>
        </td>
        <td style='vertical-align: middle;  padding: 20px 0; color: #515151; font-size: 11px;'>
            <p>#{locale.mail.header.title}</p>
	        <br/>
            <p>
	            #{locale.mail.header.description}
            </p>
        </td>
    </tr>

    <tr style='border-bottom: 1px solid rgba(196, 154, 108, 0.3);'>
        <td colspan='2' style='padding: 35px 10px; font-size: 13px; margin-bottom: 5px;'>
            <h1 style='font-size: 26px; font-weight: 700; margin-bottom: 40px;'>
                #{locale.mail.registration.thanksForInterest}
            </h1>

            <p style='min-height: 300px;'>
                #{locale.mail.registration.toFinishRegistration}
	            <a style='color: #c49a6c;' href='http://#{config.domain}/sp/shabo-club?userActivation=#{user._id}'>
                    http://#{config.domain}?userActivation=#{user._id}
                </a>
            </p>
        </td>
    </tr>

    <tr style='font-size: 11px;'>

        <td style='text-align: left; padding: 20px 10px;'>
	        <p>#{year} &copy; #{locale.mail.copyright}</p>
            <p>
	            <a style='color: #c49a6c;' href='http://shabo.ua'>shabo.ua</a>
            </p>
        </td>

        <td style='text-align: right; padding: 20px 10px;'>
	        <div class='images' style=' float: right; margin-left: 15px;'>
		        <a style='display: inline-block; width: 20px; height: 19px;' href='https://vk.com/club80330378' class='vkIcon'>
		            <img style='width: 20px; height: 19px;' src='http://shabo.ua/img/vkIcon.png' alt=''/>
                </a>
		        <a style='display: inline-block; width: 20px; height: 19px;'  href='https://www.facebook.com/SHABO.LLC' class='fbIcon'>
		            <img style='width: 20px; height: 19px;' src='http://shabo.ua/img/fbIcon.png' alt=''/>
                </a>
	        </div>
	        <div class='joinSocial'>
		        #{locale.mail.joinSocial}
	        </div>
        </td>

    </tr>
</table></body></html>"

	transporter.sendMail mailOptions, (error, info) ->

		if error
			cb null, error
		else
			if info?.error
				cb null, info.error
			else
				cb null, info



parseSignedRequestVk = (vkData, cb) ->
	socialConfig.vk.clientSecret

	sign = md5 "expire=#{vkData.expire}mid=#{vkData.mid}secret=#{vkData.secret}sid=#{vkData.sid}#{socialConfig.vk.clientSecret}"

	cb(null, sign is vkData.sig)



exports.recoverPassword = (req, res) ->

	data = req.query

	lang = getQueryLang req.originalUrl
	loc = locale[lang]

	if data.email

		async.waterfall [
			(next) ->
				Model 'User', 'findOne', next, {email: data.email}
			(doc, next) ->
				if doc
					doc.resetPassword = true

					doc.save next
				else
					next 400, res
			(doc, next) ->
				if doc
					sendResetPasswordNotification doc, loc

				View.clientSuccess {user: doc}, res

		], (err) ->
			View.clientFail err, res

	else

		View.clientFail 400, res



exports.resetPassword = (req, res) ->

	data = req.query

	if data.id

		async.waterfall [
			(next) ->
				Model 'User', 'findOne', next, {_id: data.id}
			(doc, next) ->
				if doc and doc.resetPassword is true and data.password is data.password2
					doc.password = data.password
					doc.resetPassword = false
					doc.save next
				else
					next 400, res
			(doc, next) ->
				View.clientSuccess {user: doc}, res

		], (err) ->
			View.clientFail err, res

	else

		View.clientFail 400, res


exports.getDiscount = (req, res) ->
	data = req.query
	offer = null

	lang = getQueryLang req.originalUrl
	loc = locale[lang]

	if req.user

		async.waterfall [
			(next) ->
				if data.offer_id
					Model 'Offer', 'findOne', next, {_id: data.offer_id}
				else
					next null
			(doc, next) ->
				if doc
					offer = doc

				Model 'User', 'findOne', next, {_id: req.user._id}
			(doc, next) ->
				if doc

					doc.firstName = data.name
					doc.lastName = data.surname
					doc.country = data.country
					doc.city = data.city
					doc.postAddress = data.postAddress
					doc.postIndex = data.postIndex
					doc.phone = data.phone

					if offer
						doc.offer = offer._id

					doc.save next
			(doc, next) ->
				if doc
					sendDiscount doc, loc

				View.clientSuccess {user: doc}, res
#				registeredUser = doc
#				sendRegistrationConfirmation doc, next
			(info, next) ->
				View.clientSuccess {user: registeredUser, info: info}, res

		], (err) ->
			View.clientFail err, res

sendResetPasswordNotification = (user, locale, cb) ->

	transporter = nodemailer.createTransport(smtpTransport(
		host: config.mail.host
		port: config.mail.port
		auth:
			user: config.mail.user
			pass: config.mail.pass))

	date = new Date()
	year = date.getFullYear()

	console.log "http://#{config.domain}?resetPassword=#{user._id}"

	mailOptions =
		from: config.registrationEmail
		to: "#{user.email}"
		subject: locale.mail.changePassword.title
		text: ""
		html: "<html xmlns='http://www.w3.org/1999/xhtml'>
	<head>
		<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
		<meta name='viewport' content='width=device-width, initial-scale=1.0' />
	</head>
	<body leftmargin='0' marginwidth='10' topmargin='0' marginheight='0' offset='0' width='580' style='max-width:580px;font-family:Arial,sans-serif;'>

	<table style='max-width: 570px; background-color: #f1f2f3; color:  #515151;'>

	    <tr style='border-bottom: 1px solid rgba(196, 154, 108, 0.3); vertical-align: top;'>
	        <td style=' padding: 20px 0;'>
		        <a href='http://shabo.ua'>
			        <img src='http://shabo.ua/img/emailLogo.png' alt=''/>
	            </a>
	        </td>
	        <td style='vertical-align: middle;  padding: 20px 0; color: #515151; font-size: 11px;'>
	            <p>#{locale.mail.header.title}</p>
		        <br/>
	            <p>
		            #{locale.mail.header.description}
	            </p>
	        </td>
	    </tr>

	    <tr style='border-bottom: 1px solid rgba(196, 154, 108, 0.3);'>
	        <td colspan='2' style='padding: 0 10px; font-size: 13px; margin-bottom: 5px;'>
	            <h1 style='font-size: 26px; font-weight: 700; margin-bottom: 40px;'>
	                #{locale.mail.changePassword.title}
	            </h1>

	            <p>
                  #{locale.mail.changePassword.description}
                  <br>

									<a style='color: #c49a6c;' href='http://#{config.domain}?resetPassword=#{user._id}'>
                    http://#{config.domain}?resetPassword=#{user._id}
                  </a>
	            </p>
							<br/>
	        </td>

	    </tr>

	    <tr style='font-size: 11px;'>

	        <td style='text-align: left; padding: 20px 10px;'>
		        <p>#{year} &copy; #{locale.mail.copyright}</p>
	            <p>
		            <a style='color: #c49a6c;' href='http://shabo.ua'>shabo.ua</a>
	            </p>
	        </td>

	        <td style='text-align: right; padding: 20px 10px;'>
		        <div class='images' style=' float: right; margin-left: 15px;'>
			        <a style='display: inline-block; width: 20px; height: 19px;' href='https://vk.com/club80330378' class='vkIcon'>
			            <img style='width: 20px; height: 19px;' src='http://shabo.ua/img/vkIcon.png' alt=''/>
	                </a>
			        <a style='display: inline-block; width: 20px; height: 19px;'  href='https://www.facebook.com/SHABO.LLC' class='fbIcon'>
			            <img style='width: 20px; height: 19px;' src='http://shabo.ua/img/fbIcon.png' alt=''/>
	                </a>
		        </div>
		        <div class='joinSocial'>
			        #{locale.mail.joinSocial}
		        </div>
	        </td>

	    </tr>
	</table></body></html>"

	transporter.sendMail mailOptions, (error, info) ->

		if error
			if cb
				cb null, error
		else
			if info?.error
				if cb
					cb null, info.error
			else
				if cb
					cb null, info

sendDiscount = (user, locale, cb) ->

	transporter = nodemailer.createTransport(smtpTransport(
		host: config.mail.host
		port: config.mail.port
		auth:
			user: config.mail.user
			pass: config.mail.pass))

	date = new Date()
	year = date.getFullYear()

	mailOptions =
		from: config.registrationEmail
		to: "#{user.email}, #{config.expertEmail}"
		subject: locale.mail.club.subject
		attachments: [
			filename: 'ticket.jpg'
			path: pth.join __dirname, '../public/img/', 'ticket.jpg'
		]
		text: ""
		html: "<html xmlns='http://www.w3.org/1999/xhtml'>
	<head>
		<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
		<meta name='viewport' content='width=device-width, initial-scale=1.0' />
	</head>
	<body leftmargin='0' marginwidth='10' topmargin='0' marginheight='0' offset='0' width='580' style='max-width:580px;font-family:Arial,sans-serif;'>

	<table style='max-width: 570px; background-color: #f1f2f3; color:  #515151;'>

	    <tr style='border-bottom: 1px solid rgba(196, 154, 108, 0.3); vertical-align: top;'>
	        <td style=' padding: 20px 0;'>
		        <a href='http://shabo.ua'>
			        <img src='http://shabo.ua/img/emailLogo.png' alt=''/>
	            </a>
	        </td>
	        <td style='vertical-align: middle;  padding: 20px 0; color: #515151; font-size: 11px;'>
	            <p>#{locale.mail.header.title}</p>
		        <br/>
	            <p>
		            #{locale.mail.header.description}
	            </p>
	        </td>
	    </tr>

	    <tr style='border-bottom: 1px solid rgba(196, 154, 108, 0.3);'>
	        <td colspan='2' style='padding: 0 10px; font-size: 13px; margin-bottom: 5px;'>
	            <h1 style='font-size: 26px; font-weight: 700; margin-bottom: 40px;'>
	                #{locale.mail.club.header}
	            </h1>

	            <p>
                  #{locale.mail.club.description}
	            </p>
							<br/>
	        </td>

	    </tr>
			<tr>
					<td style='padding: 0 10px; font-size: 13px; margin-bottom: 5px;'>
							<b>#{locale.mail.club.surname}:</b>
					</td>
					<td style='padding: 0 10px; font-size: 13px; margin-bottom: 5px;'>
							#{user.lastName}
					</td>
			</tr>
			<tr>
					<td style='padding: 0 10px; font-size: 13px; margin-bottom: 5px;'>
							<b>#{locale.mail.club.name}:</b>
					</td>
					<td style='padding: 0 10px; font-size: 13px; margin-bottom: 5px;'>
							#{user.firstName}
					</td>
			</tr>
			<tr>
					<td style='padding: 0 10px; font-size: 13px; margin-bottom: 5px;'>
							<b>#{locale.mail.club.country}:</b>
					</td>
					<td style='padding: 0 10px; font-size: 13px; margin-bottom: 5px;'>
							#{user.country}
					</td>
			</tr>
			<tr>
					<td style='padding: 0 10px; font-size: 13px; margin-bottom: 5px;'>
							<b>#{locale.mail.club.city}:</b>
					</td>
					<td style='padding: 0 10px; font-size: 13px; margin-bottom: 5px;'>
							#{user.city}
					</td>
			</tr>
			<tr>
					<td style='padding: 0 10px; font-size: 13px; margin-bottom: 5px;'>
							<b>#{locale.mail.club.postAddress}:</b>
					</td>
					<td style='padding: 0 10px; font-size: 13px; margin-bottom: 5px;'>
							#{user.postAddress}
					</td>
			</tr>
			<tr>
					<td style='padding: 0 10px; font-size: 13px; margin-bottom: 5px;'>
							<b>#{locale.mail.club.postIndex}:</b>
					</td>
					<td style='padding: 0 10px; font-size: 13px; margin-bottom: 5px;'>
							#{user.postIndex}
					</td>
			</tr>
			<tr>
					<td style='padding: 0 10px; font-size: 13px; margin-bottom: 5px;'>
							<b>#{locale.mail.club.phone}:</b>
					</td>
					<td style='padding: 0 10px; font-size: 13px; margin-bottom: 5px;'>
							#{user.phone}
					</td>
			</tr>

	    <tr style='font-size: 11px;'>

	        <td style='text-align: left; padding: 20px 10px;'>
		        <p>#{year} &copy; #{locale.mail.copyright}</p>
	            <p>
		            <a style='color: #c49a6c;' href='http://shabo.ua'>shabo.ua</a>
	            </p>
	        </td>

	        <td style='text-align: right; padding: 20px 10px;'>
		        <div class='images' style=' float: right; margin-left: 15px;'>
			        <a style='display: inline-block; width: 20px; height: 19px;' href='https://vk.com/club80330378' class='vkIcon'>
			            <img style='width: 20px; height: 19px;' src='http://shabo.ua/img/vkIcon.png' alt=''/>
	                </a>
			        <a style='display: inline-block; width: 20px; height: 19px;'  href='https://www.facebook.com/SHABO.LLC' class='fbIcon'>
			            <img style='width: 20px; height: 19px;' src='http://shabo.ua/img/fbIcon.png' alt=''/>
	                </a>
		        </div>
		        <div class='joinSocial'>
			        #{locale.mail.joinSocial}
		        </div>
	        </td>

	    </tr>
	</table></body></html>"

	transporter.sendMail mailOptions, (error, info) ->
		if error
			if cb
				cb null, error
		else
			if info?.error
				if cb
					cb null, info.error
			else
				if cb
					cb null, info

exports.sendFullRegistrationEmail = (req, res) ->

	data = req.body
	userId = data.userActivation
	lang = getQueryLang req.originalUrl
	loc = locale[lang]

	if userId
		async.waterfall [
			(next) ->
				Model 'User', 'findOne', next, {_id: userId}
			(doc, next) ->
				doSendFullRegistrationEmail doc, loc, next
			() ->
				View.clientSuccess {result: true}, res

		], (err) ->
			View.clientFail err, res


doSendFullRegistrationEmail = (user, locale, cb) ->

	cb null, {errors: [{responseCode: 400, response: 'No data received'}]} unless user

	transporter = nodemailer.createTransport(smtpTransport(
		host: config.mail.host
		port: config.mail.port
		auth:
			user: config.mail.user
			pass: config.mail.pass))

	date = new Date()
	year = date.getFullYear()

	mailOptions =
		from: config.registrationEmail
		to: "#{user.email}"
		subject: locale.mail.registration.subject
		text: ""
		html: "<html xmlns='http://www.w3.org/1999/xhtml'>
	<head>
		<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
		<meta name='viewport' content='width=device-width, initial-scale=1.0' />
	</head>
	<body leftmargin='0' marginwidth='10' topmargin='0' marginheight='0' offset='0' width='580' style='max-width:580px;font-family:Arial,sans-serif;'>

	<table style='max-width: 570px; background-color: #f1f2f3; color:  #515151;'>

	    <tr style='border-bottom: 1px solid rgba(196, 154, 108, 0.3); vertical-align: top;'>
	        <td style=' padding: 20px 0;'>
		        <a href='http://shabo.ua'>
			        <img src='http://shabo.ua/img/emailLogo.png' alt=''/>
	            </a>
	        </td>
	        <td style='vertical-align: middle;  padding: 20px 0; color: #515151; font-size: 11px;'>
	            <p>#{locale.mail.header.title}</p>
		        <br/>
	            <p>
		            #{locale.mail.header.description}
	            </p>
	        </td>
	    </tr>

	    <tr style='border-bottom: 1px solid rgba(196, 154, 108, 0.3);'>
	        <td colspan='2' style='padding: 35px 10px; font-size: 13px; margin-bottom: 5px;'>
	            <h1 style='font-size: 26px; font-weight: 700; margin-bottom: 40px;'>
	                #{locale.mail.registration.success}
	            </h1>

	            <p style='min-height: 300px;'>

	            </p>
	        </td>
	    </tr>

	    <tr style='font-size: 11px;'>

	        <td style='text-align: left; padding: 20px 10px;'>
		        <p>#{year} &copy; #{locale.mail.copyright}</p>
	            <p>
		            <a style='color: #c49a6c;' href='http://shabo.ua'>shabo.ua</a>
	            </p>
	        </td>

	        <td style='text-align: right; padding: 20px 10px;'>
		        <div class='images' style=' float: right; margin-left: 15px;'>
			        <a style='display: inline-block; width: 20px; height: 19px;' href='https://vk.com/club80330378' class='vkIcon'>
			            <img style='width: 20px; height: 19px;' src='http://shabo.ua/img/vkIcon.png' alt=''/>
	                </a>
			        <a style='display: inline-block; width: 20px; height: 19px;'  href='https://www.facebook.com/SHABO.LLC' class='fbIcon'>
			            <img style='width: 20px; height: 19px;' src='http://shabo.ua/img/fbIcon.png' alt=''/>
	                </a>
		        </div>
		        <div class='joinSocial'>
			        #{locale.mail.joinSocial}
		        </div>
	        </td>

	    </tr>
	</table></body></html>"

	transporter.sendMail mailOptions, (error, info) ->
		if error
			cb null, error
		else
			if info?.error
				cb null, info.error
			else
				cb null, info
