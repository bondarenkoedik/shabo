async = require 'async'
_ = require 'underscore'
moment = require 'moment'

Logger = require './logger'

exports.render = render = (name, res, data)->
	data or= {}

	res.render name, data
		

exports.error = (err, res)->
	data =
		success: false
		error: err.message
		code: err.code

	render 'admin/main/error/index', res, data

exports.clientError = (err, res) ->
	data =
		success: false
		error: err.message
		code: err.code

	res.send data


exports.clientSuccess = (data, res) ->	
	res.send _.extend data, success: true

exports.clientFail = (err, res)->
	res.status 500

	data =
		success: false
		err: err

	res.json data

exports.globals = (req, res, next)->
	res.locals.defLang = 'ru'
	res.locals.lang = req.lang

	if req.user
		res.locals.euser = req.user
		res.locals.user = req.user

	res.locals.moment = moment

	next()