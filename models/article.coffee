mongoose = require 'mongoose'

ObjectId = mongoose.Schema.Types.ObjectId
Mixed = mongoose.Schema.Types.Mixed

SchemaFields =
	lang: [
		languageId:
			type: ObjectId
			ref: 'Language'
		title:
			type: String
			trim: true
		shortDescription:
			type: String
			trim: true
		content:
			type: String
			trim: true
	]
	position:
		type: Number
		trim: true
	link:
		type: String
		trim: true
	view:
		type: String
		trim: true
	ready:
		type: String
	createdAt:
		type: Date
		default: Date.now
	postDate:
		type: Date
		default: Date.now
	img:
		type: String
		default: ''
	titleColor:
		type: String
		default: '#ffffff'
	textColor:
		type: String
		default: '#ffffff'
	articleCategory:
		type: ObjectId
		ref: 'ArticleCategory'
	author:
		type: ObjectId
		ref: 'Author'
	publicationDate:
		type: Number
		trim: true
		default: Date.now
	sortDate:
		type: Number
		trim: true
		index: true


options =
	collection: 'articles'

Schema = new mongoose.Schema SchemaFields, options

module.exports =  mongoose.model 'Article', Schema
