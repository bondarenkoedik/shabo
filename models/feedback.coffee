mongoose = require 'mongoose'

ObjectId = mongoose.Schema.Types.ObjectId
Mixed = mongoose.Schema.Types.Mixed

SchemaFields =
	content:
		type: String
		trim: true
	date:
		type: Number
		default: Date.now
	user:
		type: ObjectId
		ref: 'User'

options =
	collection: 'feedback'

Schema = new mongoose.Schema SchemaFields, options

module.exports =  mongoose.model 'Feedback', Schema
