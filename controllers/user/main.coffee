async = require 'async'
_ = require 'underscore'
nodemailer = require 'nodemailer'
smtpTransport = require 'nodemailer-smtp-transport'

View = require '../../lib/view'
Model = require '../../lib/model'
socialConfig = require '../../meta/socialConfig'
config = require '../../meta/config'

locale = require '../../locale'

getQueryLang = (req, res) ->
	return req.cookies.language if req.cookies.language
	lang = (req?.headers?["accept-language"] || 'ua').substring(0,2)
	if lang is 'uk' then lang = 'ua'
	return lang

exports.index = (req, res) ->

	if req.query.userActivation
		activateUser req, res

	user = getUser req.user
	normalizeArticles()

	lang = getQueryLang req, res
	loc = locale[lang]

	View.render 'user/index', res,
		locale: loc
		lang: if lang is 'ru' then '' else lang
		socialConfig: socialConfig
		user: user

normalizeArticles = ->
	async.waterfall [
		(next) ->
			Model 'Article', 'find', next, {}, {'lang': 0}, {sort: {sortDate: 1}}
		(docs, next) ->
			docs.forEach (article) ->

				if article.publicationDate?.toString().length > 10
					date = new Date(article.publicationDate)
				else
					date = new Date(article.postDate)

				month = date.getMonth()+1;
				month = month.toString()
				if month.length < 2
					month = '0' + month;

				day = date.getDate()+1;
				day = day.toString()
				if day.length < 2
					day = '0' + day;

				article.sortDate = '' + date.getFullYear() + month + day + ''
				article.save()

	], (err) ->
		console.error err

getUser = (user) ->
	return null unless user

	userResult = _.extend {}, user
	delete userResult.password
	userResult


activateUser = (req, res) ->
	id = req.query.userActivation
	lang = getQueryLang req, res
	loc = locale[lang]

	async.waterfall [
		(next) ->
			Model 'User', 'findOne', next, _id: id
		(doc, next) ->
			if doc
				doc.status = 1
				sendRegistrationConfirmation doc, loc
				doc.save next
			else
				next
	], (err) ->
		console.error err

exports.getPage = (req, res) ->
	reqLang = getQueryLang req, res

	async.parallel
		page: (proceed) ->
			Page = Model 'Page', 'findOne', null, link: req.params.link
			Page.lean().exec proceed
		lang: (proceed) ->
			Language = Model 'Language', 'findOne', null, isoCode: reqLang
			Language.lean().exec proceed
	, (err, data) ->

		unless data.page
			return View.clientFail 'Such link is not found', res

		data.page.lang = _.find data.page.lang, (l) ->
			return l.languageId.toString() == data.lang._id.toString()

		View.clientSuccess data: data.page, res

exports.ie = (req, res) ->
	View.render 'user/ie', res, {}


sendRegistrationConfirmation = (user, locale, cb) ->
	cb null, {errors: [{responseCode: 400, response: 'No data received'}]} unless user

	date = new Date()
	year = date.getFullYear()

	transporter = nodemailer.createTransport(smtpTransport(
		host: config.mail.host
		port: config.mail.port
		auth:
			user: config.mail.user
			pass: config.mail.pass))

	mailOptions =
		from: config.registrationEmail
		to: "#{user.email}"
		subject: locale.mail.registration.subject
		text: "http://#{config.domain}?userActivation=#{user._id}"
		html: "<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	<meta name='viewport' content='width=device-width, initial-scale=1.0' />
</head>
<body leftmargin='0' marginwidth='10' topmargin='0' marginheight='0' offset='0' width='580' style='max-width:580px;font-family:Arial,sans-serif;'>

<table style='max-width: 570px; background-color: #f1f2f3; color:  #515151;'>

    <tr style='border-bottom: 1px solid rgba(196, 154, 108, 0.3); vertical-align: top;'>
        <td style=' padding: 20px 0;'>
	        <a href='http://shabo.ua'>
		        <img src='http://shabo.ua/img/emailLogo.png' alt=''/>
            </a>
        </td>
        <td style='vertical-align: middle;  padding: 20px 0; color: #515151; font-size: 11px;'>
            <p>#{locale.mail.header.title}</p>
	        <br/>
            <p>
							#{locale.mail.header.description}
            </p>
        </td>
    </tr>

    <tr style='border-bottom: 1px solid rgba(196, 154, 108, 0.3);'>
        <td colspan='2' style='padding: 35px 10px; font-size: 13px; margin-bottom: 5px;'>
            <h1 style='font-size: 26px; font-weight: 700; margin-bottom: 40px;'>
                #{locale.mail.registration.success}
            </h1>

            <p style='min-height: 300px;'>

            </p>
        </td>
    </tr>

    <tr style='font-size: 11px;'>

        <td style='text-align: left; padding: 20px 10px;'>
	        <p>#{year} &copy; #{locale.mail.copyright}</p>
            <p>
	            <a style='color: #c49a6c;' href='http://shabo.ua'>shabo.ua</a>
            </p>
        </td>

        <td style='text-align: right; padding: 20px 10px;'>
	        <div class='images' style=' float: right; margin-left: 15px;'>
		        <a style='display: inline-block; width: 20px; height: 19px;' href='https://vk.com/club80330378' class='vkIcon'>
		            <img style='width: 20px; height: 19px;' src='http://shabo.ua/img/vkIcon.png' alt=''/>
                </a>
		        <a style='display: inline-block; width: 20px; height: 19px;'  href='https://www.facebook.com/SHABO.LLC' class='fbIcon'>
		            <img style='width: 20px; height: 19px;' src='http://shabo.ua/img/fbIcon.png' alt=''/>
                </a>
	        </div>
	        <div class='joinSocial'>
		        #{locale.mail.joinSocial}
	        </div>
        </td>

    </tr>
</table></body></html>"

	transporter.sendMail mailOptions, (error, info) ->
		if error
			if cb
				cb null, error
		else
			if info?.error
				if cb
					cb null, info.error
			else
				if cb
					cb null, info
