Crud = require '../../lib/crud'

crud = new Crud
  modelName: 'Country'

module.exports.rest = crud.request.bind crud
module.exports.restFile = crud.fileRequest.bind crud
