Crud = require '../../lib/crud'
async = require 'async'
Model = require '../../lib/model'

class FeedbackCrud extends Crud
	add: (req, cb) ->
		return cb 403 unless req.user

		data = req.query
		data.user = req.user._id

		async.waterfall [
			(next) =>
				DocModel = @DataEngine()
				doc = new DocModel data
				doc.save next
			(doc, next) =>
				if doc
					if req.files
						@saveDocumentFiles doc, req, cb
					else
						cb null, _id: data._id
				else
					cb 'Document was not saved'
		], cb

crud = new FeedbackCrud
	modelName: 'Feedback'
	relatedModels: [
		name: 'User'
		field: 'user'
		findAllInRelated: false
		cascade: false
	]


module.exports.rest = crud.request.bind crud
module.exports.restFile = crud.fileRequest.bind crud