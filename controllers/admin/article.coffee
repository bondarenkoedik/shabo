Crud = require '../../lib/crud'
Model = require '../../lib/model'
async = require 'async'
_ = require 'lodash'
moment = require 'moment'

class ArticleCrud extends Crud
	getRelated: (query, cb, options = {}, fields = null) ->
		sort = false
		if options.sort
			sort = options.sort
			delete options.sort

		if fields
			documents = @DataEngine 'find', null, fields, query, {'lang.content': 0}
		else
			documents = Model 'Article', 'find', null, query, fields, options

		populateString = ''

		for model in @options.relatedModels
			populateString += model.field + ' '
		populateString = populateString.trim()
		if sort
			documents.sort sort

		documents.populate(populateString).exec cb

	add: (req, cb) ->
		data = req.body

		data.sortDate = @getSortDate data.publicationDate
		data.publicationDate = @convertDate data.publicationDate if data.publicationDate

		async.waterfall [
			(next) =>
				DocModel = @DataEngine()
				doc = new DocModel data
				doc.save next
			(doc, next) =>
				if doc
					if req.files
						@saveDocumentFiles doc, req, cb
					else
						cb null, _id: data._id
				else
					cb 'Document was not saved'
		], cb

	update: (id, data, cb) ->

		data.sortDate = @getSortDate data.publicationDate
		data.publicationDate = @convertDate data.publicationDate if data.publicationDate

		async.waterfall [
			(next) =>
				@DataEngine 'findById', next, id
			(doc, next) =>
				_.extend doc, data
				doc.save cb
		], cb

	convertDate: (publicationDate) ->
		dateArray = publicationDate.split('.')
		date = new Date(dateArray[2], dateArray[1] - 1, dateArray[0])

		return date.getTime()

	getSortDate: (publicationDate) ->
		if publicationDate
			dateArray = publicationDate.split('.')
			date = new Date(dateArray[2], dateArray[1] - 1, dateArray[0])
		else
			date = new Date()

		month = date.getMonth()+1;
		month = month.toString()
		if month.length < 2
			month = '0' + month;

		day = date.getDate()+1;
		day = day.toString()
		if day.length < 2
			day = '0' + day;

		return '' + date.getFullYear() + month + day + ''

crud = new ArticleCrud
	modelName: 'Article'
	relatedModels: [
		name: 'ArticleCategory'
		field: 'articleCategory' # related documents will be populated into variable, named as "field" variable
		findAllInRelated: true # findAll will be returned in variable, named as "name" above
		cascade: false
	,
		name: 'Author'
		field: 'author' # related documents will be populated into variable, named as "field" variable
		findAllInRelated: true # findAll will be returned in variable, named as "name" above
		cascade: false
	]
	files: [
		name: 'img'
		replace: true
		type: 'string'
		sizes: [600, 1024]
	]

module.exports.rest = crud.request.bind crud
module.exports.restFile = crud.fileRequest.bind crud