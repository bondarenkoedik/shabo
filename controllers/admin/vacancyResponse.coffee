Crud = require '../../lib/crud'
Model = require '../../lib/model'
async = require 'async'
_ = require 'lodash'
nodemailer = require 'nodemailer'
smtpTransport = require 'nodemailer-smtp-transport'
config = require '../../meta/config'
locale = require '../../locale'

getQueryLang = (url) ->
	myRegexp = /^\/(ru|ua|en)/
	match = myRegexp.exec url
	return match?[1] or 'ru'

class VacancyResponseCrud extends Crud

	add: (req, cb) ->
		data = req.body

		console.log 'add'

		lang = getQueryLang req.originalUrl
		loc = locale[lang]

		async.waterfall [
			(next) =>
				DocModel = @DataEngine()
				doc = new DocModel data
				doc.save next
			(doc, next) =>
				if doc
					if req.files
						@saveDocumentFiles doc, req, (document) ->
							console.log 'sendVacancyResponse'
							sendVacancyResponse doc, req.user, loc, cb
					else
						cb null, _id: data._id
				else
					cb 'Document was not saved'
		], cb


sendVacancyResponse = (doc, user, locale, cb) ->

	date = new Date()
	year = date.getFullYear()

	transporter = nodemailer.createTransport(smtpTransport(
		host: config.mail.host
		port: config.mail.port
		auth:
			user: config.mail.user
			pass: config.mail.pass))

	mailOptions =
		from: config.registrationEmail
		to: "#{config.expertEmail}, #{config.hrEmail}"
		subject: 'Оставлен новый отклик на вакансию'
		html: "<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	<meta name='viewport' content='width=device-width, initial-scale=1.0' />
</head>
<body leftmargin='0' marginwidth='10' topmargin='0' marginheight='0' offset='0' width='580' style='max-width:580px;font-family:Arial,sans-serif;'>

<table style='max-width: 570px; background-color: #f1f2f3; color:  #515151;'>

    <tr style='border-bottom: 1px solid rgba(196, 154, 108, 0.3); vertical-align: top;'>
        <td style=' padding: 20px 0;'>
	        <a href='http://shabo.ua'>
		        <img src='http://shabo.ua/img/emailLogo.png' alt=''/>
            </a>
        </td>
        <td style='vertical-align: middle;  padding: 20px 0; color: #515151; font-size: 11px;'>
            <p>#{locale.mail.header.title}</p>
	        <br/>
            <p>
							#{locale.mail.header.description}
            </p>
        </td>
    </tr>

    <tr style='border-bottom: 1px solid rgba(196, 154, 108, 0.3);'>
        <td colspan='2' style='padding: 35px 10px; font-size: 13px; margin-bottom: 5px;'>
            <h1 style='font-size: 26px; font-weight: 700; margin-bottom: 40px;'>
                Оставлен новый отклик на вакансию
            </h1>

            <p style='min-height: 300px;'>
	            <a style='color: #c49a6c;' href='http://#{config.domain}/uploads/#{doc.cv}'>
                    http://#{config.domain}/uploads/#{doc.cv}
                </a>
            </p>
        </td>
    </tr>

    <tr style='font-size: 11px;'>

        <td style='text-align: left; padding: 20px 10px;'>
	        <p>#{year} &copy; #{locale.mail.copyright}</p>
            <p>
	            <a style='color: #c49a6c;' href='http://shabo.ua'>shabo.ua</a>
            </p>
        </td>

        <td style='text-align: right; padding: 20px 10px;'>
	        <div class='images' style=' float: right; margin-left: 15px;'>
		        <a style='display: inline-block; width: 20px; height: 19px;' href='https://vk.com/club80330378' class='vkIcon'>
		            <img style='width: 20px; height: 19px;' src='http://shabo.ua/img/vkIcon.png' alt=''/>
                </a>
		        <a style='display: inline-block; width: 20px; height: 19px;'  href='https://www.facebook.com/SHABO.LLC' class='fbIcon'>
		            <img style='width: 20px; height: 19px;' src='http://shabo.ua/img/fbIcon.png' alt=''/>
                </a>
	        </div>
	        <div class='joinSocial'>
		        #{locale.mail.joinSocial}
	        </div>
        </td>

    </tr>
</table></body></html>"

	transporter.sendMail mailOptions, (error, info) ->
		if error
			if cb
				cb null, error
		else
			if info?.error
				if cb
					cb null, info.error
			else
				if cb
					cb null, info


crud = new VacancyResponseCrud
	modelName: 'VacancyResponse'
	relatedModels: [
		name: 'Vacancy'
		field: 'vacancy' # related documents will be populated into variable, named as "field" variable
		findAllInRelated: true # findAll will be returned in variable, named as "name" above
		cascade: false
	]
	files: [
		name: 'cv'
		replace: true
		type: 'string'
	]

module.exports.rest = crud.request.bind crud
module.exports.restFile = crud.fileRequest.bind crud
