'use strict';

(function(){

	var component = 'foodItem',
		componentUppercase = 'FoodItem',
		componentPlural = 'food';

	define(
		[
			'canjs',
			'lib/edit/edit'
		],

		function (can, Edit) {

			return Edit.extend({
				defaults: {
					viewpath: 'modules/'+componentPlural+'/views/',

					moduleName: component,

					successMsg: 'Успешно сохранен.',
					errorMsg: 'Ошибка сохранения.',

					form: '.set'+componentUppercase
				}
			}, {});

		}
	);

}());
