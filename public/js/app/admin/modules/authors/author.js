'use strict';

(function(){

	var component = 'author',
		componentUppercase = 'Author',
		componentPlural = 'authors';

	define(
		[
			'canjs',
			'lib/edit/edit',
			'translit'
		],

		function (can, Edit) {

			return Edit.extend({
				defaults: {
					viewpath: 'modules/'+componentPlural+'/views/',

					moduleName: component,

					successMsg: 'Успешно сохранено.',
					errorMsg: 'Ошибка сохранения.',

					form: '.set'+componentUppercase
				}
			}, {});

		}
	);

}());
