'use strict';

(function (){

	var component = 'product',
		componentUppercase = 'Product',
		componentPlural = 'products';

	define(
		['canjs', 'lib/list/list', 'modules/'+componentPlural+'/'+component+'', 'modules/'+componentPlural+'/'+componentPlural+'Model'],

		function (can, List, SingleUnit, Model) {

			return List.extend({
				defaults: {
					viewpath: 'app/modules/'+componentPlural+'/views/',

					Edit: SingleUnit,

					moduleName: componentPlural,
					Model: Model,

					deleteMsg: 'Вы действительно хотите удалить эту запись?',
					deletedMsg: 'Запись успешно удалена!',

					add: '.add'+componentUppercase+'',
					edit: '.edit'+componentUppercase+'',

					remove: '.remove'+componentUppercase+'',

					formWrap: '.'+component+'Form',

					parentData: '.'+component
				}
			}, {
				getExistingInstance: function (id) {
					var options = this.options,
						doc = _.find(this.module.attr(options.moduleName), function (doc) {
							return doc && doc.attr('_id') === id;
						});
					if (doc) {
						var food = _.map(doc.attr('food'), function (foodItem) {
							return foodItem.attr('_id');
						});
						doc.attr('food', food);
					}
					return doc;
				}
			});

		}
	);

}());
