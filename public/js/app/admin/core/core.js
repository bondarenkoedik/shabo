require.config({
	baseUrl: '/js/lib',
	urlArgs: 'v=0.1.66',
	paths: {
		cssDir: '../../css/admin',
		app: '../app/admin',
		lib: '../app/admin/lib',
		can: 'canjs/amd/can/',
		canjs: 'canjs/amd/can',
		core: '../app/admin/core',
		jquery: 'jquery/dist/jquery',
		modules: '../app/admin/modules',
		funcunit: 'funcunit/dist/funcunit',
		underscore: 'underscore/underscore',
		components: '../app/admin/components',
		adminLTE: 'admin-lte/js/AdminLTE/app',
		bootstrap: 'admin-lte/js/bootstrap.min',
		cssComponents: '../../css/admin/components',
		summernote: 'summernote/plugin/summernote-ext-video',
		'colorpicker': 'colorpicker/js/colorpicker',
		select2: 'select2/dist/js/select2',
		datepicker: 'bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min',
		translit: 'synctranslit/js/jquery.synctranslit',
		multisortable: 'jquery.multisortable/src/jquery.multisortable'
	},
	map: {
		'*': {
			'css': 'require-css/css'
		}
	},
	shim: {
		'jquery': {
			exports: '$'
		},
		'underscore': {
			exports: '_'
		},
		'funcunit': {
			exports: 'F'
		},
		'canjs': {
			deps: [
				'jquery',
				'can/route/pushstate',
				'can/map/define',
				'can/map/delegate',
				'can/map/sort',
				'can/list/promise',
				'can/construct/super'
			]
		},
		'components/upload/upload': {
			deps: ['jquery-form/jquery.form']
		},
		'bootstrap': {
			deps: ['jquery']
		},
		'admin-lte/js/jquery-ui-1.10.3.min': {
			deps: ['jquery']
		},
		'adminLTE': {
			deps: [
				'jquery',
				'bootstrap',
				'admin-lte/js/jquery-ui-1.10.3.min'
			]
		},
		summernote: {
			format: 'global',
			deps: [
				'jquery',
				'bootstrap',
				'/js/lib/summernote/summernote.min.js'
			]
		},
		'colorpicker': {
			format: 'global',
			deps: [
				'css!/js/lib/colorpicker/css/colorpicker.css'
			]
		},
		select2: {
			format: 'global',
			deps: [
				'css!/js/lib/select2/dist/css/select2.css'
			]
		},
		datepicker: {
			format: 'global',
			deps: [
				'/js/lib/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
				'css!/js/lib/bootstrap-datepicker/dist/css/bootstrap-datepicker.css'
			]
		}
	}
});

require([
		'app/router/router',
		'components/notification/notification',
		'core/config',

		'adminLTE',
		'core/appState',
		'components/tabs/tabs',
		'components/upload/upload',
		'core/viewHelpers',

		'css!cssDir/base',
		'summernote'
	],
	function (Router, initNotification, config) {

		new Router(document.body, config.router);
		initNotification();

	}
);