define([
	'canjs'
],
	function (can) {

		var AppState = can.Map.extend({
			define: {
				notification: {
					value: {},
					serialize: false
				}
			},
			langs: [
				{
					"_id" : "53db9c277ae86638de0fc923",
					"default" : true,
					"active" : true,
					"isoCode" : "ru",
					"name" : "Русский"
				},
				{
					"_id" : "53db9bd57ae86638de0fc922",
					"default" : false,
					"active" : true,
					"isoCode" : "ua",
					"name" : "Українська"
				},
				{
					"_id" : "53db9bd57ae86638de0fc924",
					"default" : false,
					"active" : true,
					"isoCode" : "en",
					"name" : "English"
				}
			],
			locale: window.locale,
			uploadPath: '/uploads/'
		});

		return new AppState();
	}
);
