'use strict';

(function (){

    var component = 'auth';

    define([
            'canjs',
            'core/appState',
            'velocity',
            'jquery-form/jquery.form'
        ],
        function (can, appState) {

            return can.Control.extend({
                defaults: {
                    viewpath: 'app/modules/'+component+'/views/',
                    facebookPermissions: '',
                    vkLoginPermissions: ''
                },

                register: function (el, cb) {
                    var self = this,
                        form = el.ajaxSubmit({
                            method: 'POST',
                            url: '/user/register'
                        });

                    form.data('jqxhr').done(function (data) {
                        cb(data);
                    }).fail(function (data) {
                        cb(data);
                    });
                },

                login: function (el, cb) {
                    var self = this,
                        form = el.ajaxSubmit({
                            method: 'POST',
                            url: '/user/login'
                        });

                    form.data('jqxhr').done(function (data) {
                        self.loginSuccess(data, cb);
                    }).fail(function (data) {
                        cb(data);
                    });
                },

                logout: function (cb) {
                    can.ajax({
                        url: '/user/logout',
                        type: 'POST'
                    }).done(function (data) {
                        $('.showRegistrationPopup').css('display', 'inline');
                        $('.logoutButton').css('display', 'none');
                    }).fail(function (data) {
                        cb(data);
                    });
                },

                loginSuccess: function (data, cb) {
                    appState.attr('user', data);
                    cb(data);
                },

                facebookLogin: function (cb) {
                    var self = this;

                    FB.login(function(response){
                        self.fbLoginResponse(response, cb);
                    }, {
                        scope: self.defaults.facebookPermissions
                    });
                },

                fbLoginResponse: function (response, cb) {

                    var self = this;

                    if (response.status === 'connected') {

                        FB.api('/me', function(userResponse) {

                            response.user = userResponse;

                            FB.api(
                                "/me/picture?width=180&height=180",
                                function (imageResponse) {
                                    if (imageResponse && !imageResponse.error) {
                                        response.user.image = imageResponse.data.url;
                                    }

                                    can.ajax({
                                        url: '/user/facebook',
                                        type: 'POST',
                                        data: {
                                            response: response
                                        }
                                    }).done(function (data) {
                                        self.loginSuccess(data, cb);
                                    }).fail(function (data) {
                                        console.error(data);
                                    });
                                }
                            );
                        });

                    } else if (response.status === 'not_authorized') {
                        // The person is logged into Facebook, but not your app.
                    } else {
                        // The person is not logged into Facebook, so we're not sure if
                        // they are logged into this app or not.
                    }
                },

                vkLogin: function (cb) {
                    var self = this;

                    VK.Auth.login(function(response) {
                        self.vkLoginResponse(response, cb);

                    }, self.defaults.vkLoginPermissions);
                },

                vkLoginResponse: function (response, cb) {
                    var self = this;

                    if (response.session) {

                        VK.api("getProfiles", {
                            uids: response.session.mid,
                            fields: 'photo_200'
                        }, function (profileData){
                            if (profileData && profileData.response[0]) {
                                response.session.user.image = profileData.response[0].photo_200;
                            }

                            can.ajax({
                                url: '/user/vk',
                                type: 'POST',
                                data: {
                                    response: response
                                }
                            }).done(function (data) {
                                self.loginSuccess(data, cb);
                            }).fail(function (data) {
                                console.error(data);
                            });
                        });

                    } else {
                        alert('not auth');
                    }
                },

                recoverPassword: function (el, cb) {
                    var self = this,
                        form = el.ajaxSubmit({
                            method: 'POST',
                            url: '/user/recoverPassword'
                        });

                    form.data('jqxhr').done(function (data) {
                        cb(data);
                    }).fail(function (data) {
                        cb(data);
                    });
                },

                getDiscount: function(el, cb) {
                    var self = this,
                        form = el.ajaxSubmit({
                            method: 'POST',
                            url: '/user/getDiscount'
                        });

                    form.data('jqxhr').done(function (data) {
                        cb(data);
                    }).fail(function (data) {
                        cb(data);
                    });
                },

                sendFullRegistrationEmail: function (data, cb) {
                    can.ajax({
                        url: '/user/sendFullRegistrationEmail',
                        type: 'POST',
                        data: data,
                        success: function (data) {
                            if (cb) {
                                cb(data);
                            }
                        }
                    });
                },

                resetPassword: function (el, cb) {
                    var self = this,
                        form = el.ajaxSubmit({
                            method: 'POST',
                            url: '/user/resetPassword',
                            data: {id: appState.attr('resetPassword')}
                        });

                    form.data('jqxhr').done(function (data) {
                        cb(data);
                    }).fail(function (data) {
                        cb(data);
                    });
                }

            }, {
                init: function () {

                }
            });

        }
    );
})();