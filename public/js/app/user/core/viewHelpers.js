'use strict';

var entityMap = {
	"&": "&amp;",
	"<": "&lt;",
	">": "&gt;",
	'"': '&quot;',
	"'": '&#39;',
	"/": '&#x2F;'
};

function escapeTags(string) {
	return String(string).replace(/<(?:.|\n)*?>/gm, '');
}

define([
		'canjs',
		'core/appState'
	],
	function (can, appState, $clamp) {

		can.mustache.registerHelper('getLang', function (lang, key, ellipsis, crop, stripBr, escapeHtml) {
			var langsArray = ['ru', 'ua', 'en'];
			var result = '';

			if (typeof lang === 'function') {
				if (appState.attr('lang') == '') {
					if (lang() && lang()[0]) {
						result = lang()[0][key];
					}
				} else if (appState.attr('lang') == 'ua') {
					if (lang()[1]) {
						result = lang()[1][key];
					}
				} else if (appState.attr('lang') == 'en') {
					if (lang()[2]) {
						result = lang()[2][key];
					}
				}
			} else {
				if (appState.attr('lang') == '') {
					if (lang[0]) {
						result = lang[0][key];
					}
				} else if (appState.attr('lang') == 'ua') {
					if (lang[1]) {
						result = lang[1][key];
					}
				} else if (appState.attr('lang') == 'en') {
					if (lang[2]) {
						result = lang[2][key];
					}
				}
			}

			if (ellipsis) {
				if (result && result.length > crop) {
					result = result.substring(0, crop);
					result += '...';
				}
			}

			if (stripBr) {
				result = stripBrString(result);
			}
			if (escapeHtml) {
				result = escapeTags(result);
			}

			return result;
		});

/*        can.mustache.registerHelper('renderRecommendations', function (productFood) {
            var html = '';
            if (productFood && productFood() && productFood().length > 0) {
                productFood().attr().forEach(function(element, index){
                    html += '<img src="'+appState.attr("imgPath") + 'rec' + element +'.png">';
                });
            }

            return html;
        });*/

		can.mustache.registerHelper('getDate', function(unixTimestamp) {
			var timestamp = '';

			if (unixTimestamp) {
				if (typeof unixTimestamp === 'function') {

					if (unixTimestamp()) {
						if (unixTimestamp().length == 13) {
							var date = new Date(parseInt(unixTimestamp()));
						} else {
							var date = new Date(unixTimestamp());
						}

						if (date == 'Invalid Date') {
							timestamp = unixTimestamp();
						} else {
							var day = date.getDate() < 10 ? '0'+date.getDate() : date.getDate();
							var month = date.getMonth() + 1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1;
							var year = date.getFullYear();

							timestamp = day+'.'+month+'.'+year;
						}
					}

				} else {
					var date = new Date(unixTimestamp());

					var day = date.getDate() < 10 ? '0'+date.getDay() : date.getDay();
					var month = date.getMonth() + 1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1;
					var year = date.getFullYear();

					timestamp = day+'.'+month+'.'+year;
				}
			}

			return timestamp;
		});

		can.mustache.registerHelper('getCommentsAmount', function(comments) {
			var amount = null;

			if (comments && comments()) {

			}

			return amount;
		});

		can.mustache.registerHelper('incrementIndex', function(index) {
			return index + 1;
		});

		can.mustache.registerHelper('getCurrentYear', function(comments) {
			var year = null;

			var date = new Date();
			year = date.getFullYear();

			return year;
		});

		can.mustache.registerHelper('stripBr', function(string) {
			var result = string();

			return stripBrString(result);
		});

		var stripBrString = function (string) {
			var result = false;
			if (string) {
				result = string.replace(/<br>/g, '');
				result = result.replace(/<br >/g, '');
				result = result.replace(/<br \/>/g, '');
			}

			return result;
		};

		can.mustache.registerHelper('getCleared', function(index) {
			if ((index + 1) % 3 == 0 ) {
				return '<section class="clearfix"></section>'
			}
		});

		function smColumn (string, array, langIndex) {
			string += '<div class="smColumn">';

			for (var i = 0; i < 2; i++) {
				if (array[i]) {
					var image = '/uploads/' + array[i].img.main;
					string += '' +
						'<a class="module tileItem" href="productCategory/' + array[i].link + '">' +
						'<div class="tileBg" style="background-image: url(' + image + ')">' +
						'<div class="darkBg"></div>' +
						'</div>' +
						'<div class="header">' + array[i].lang[langIndex].title + '</div>' +
						'</a>'
				}
			}
			string += '</div>';

			return string;
		}

		function lgColumn(string, array, langIndex) {
			string += '<div class="lgColumn">';
			for (var i = 0; i < 6; i++) {
				if (array[i]) {
					var image = '/uploads/' + array[i].img.main;
					string += '' +
						'<a class="module tileItem" href="productCategory/' + array[i].link + '">' +
						'<div class="tileBg" style="background-image: url(' + image + ')">' +
						'<div class="darkBg"></div>' +
						'</div>' +
						'<div class="header">' + array[i].lang[langIndex].title + '</div>' +
						'</a>';
				}
			}
			string += '</div>';

			return string;
		}

		function fullLeftColumn (string, array, langIndex) {
			string += '<div class="fullColumn">';
			for (var i = 0; i < 3; i++) {
				if (array[i]) {
					var image = '/uploads/' + array[i].img.main;
					string += '' +
						'<a class="module tileItem" href="productCategory/' + array[i].link + '">' +
						'<div class="tileBg" style="background-image: url(' + image + ')">' +
						'<div class="darkBg"></div>' +
						'</div>' +
						'<div class="header">' + array[i].lang[langIndex].title + '</div>' +
						'</a>';
				}
			}
			string += '</div>';

			return string;
		}

		function renderDesktopCategories (categories, langIndex) {
			var result = '';

			result = smColumn(result, categories.slice(0, 2), langIndex);

			if (categories.length >= 2) {
				result = lgColumn(result, categories.slice(2, 8), langIndex);
			}
			if (categories.length >= 8) {
				result = lgColumn(result, categories.slice(8, 14), langIndex);
			}
			if (categories.length >= 14) {
				result = smColumn(result, categories.slice(14, 16), langIndex);
			}

			var i = 16;
			while (i < categories.length) {

				result = fullLeftColumn(result, categories.slice(i, i+3), langIndex);

				i += 6;
			}

			return result;
		}

		function renderTabletCategories (categories, langIndex) {
			var result = '';

			return result;
		}

		function renderMobileCategories (categories, langIndex) {
			var result = '';

			return result;
		}

		can.mustache.registerHelper('renderCategories', function(categories) {
			var result = '';
			var width = window.innerWidth;
			var lang = appState.attr('lang');
			var langIndex = 0;
			if (lang === 'ua') {
				langIndex = 1;
			} else if (lang === 'en') {
				langIndex = 2;
			}

			if (width >= 1024) {
				result = renderDesktopCategories(categories, langIndex);
			} else if (width >= 768) {
				result = renderTabletCategories(categories, langIndex);
			} else {
				result = renderMobileCategories(categories, langIndex);
			}

			return result;
		});
	}
);

function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}