define({
	router: {
		base: '/',
		modulesContainer: '#modules',
		routes: [{
			route: ':module',
			defaults: {
				module: 'sp',
				id: 'main-page'
			}
		}],
		modules: [{
			name: 'sp',
			path: 'app/modules/simplePage/simplePage'
		}, {
			name: 'author',
			path: 'app/modules/author/author'
		},{
			name: 'ua',
			path: 'app/modules/simplePage/simplePage'
		}, {
			name: 'productCategories',
			path: 'app/modules/productCategories/productCategories'
		}, {
			name: 'products',
			path: 'app/modules/products/products'
		}, {
			name: 'articleCategories',
			path: 'app/modules/articleCategories/articleCategories'
		}, {
			name: 'articles',
			path: 'app/modules/articles/articles'
		}, {
			name: 'article',
			path: 'app/modules/article/article'
		}, {
			name: 'awards',
			path: 'app/modules/awards/awards'
		}, {
			name: 'gallery',
			path: 'app/modules/galleryImages/galleryImages'
		}, {
			name: 'vacancies',
			path: 'app/modules/vacancies/vacancies'
		}, {
			name: 'videos',
			path: 'app/modules/galleryVideos/galleryVideos'
		}, {
			name: 'contacts',
			path: 'app/modules/contacts/contacts'
		}, {
			name: 'productCategory',
			path: 'app/modules/productCategory/productCategory'
		}, {
			name: 'feedback',
			path: 'app/modules/feedback/feedback'
		}, {
			name: 'faqs',
			path: 'app/modules/faqs/faqs'
		}]
	}
});
