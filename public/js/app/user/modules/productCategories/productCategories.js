'use strict';

(function(){

	var component = 'productCategories';

	define(
		[
			'canjs',
			'modules/'+component+'/'+component+'Model',
			'core/appState',
			'css!app/modules/'+component+'/css/'+component+'.css'
		],

		function (can, ComponentModel, appState) {

			return can.Control.extend({
				defaults: {
					viewpath: 'app/modules/'+component+'/views/'
				}
			}, {
				init: function () {
					var self = this;

					ComponentModel.findAll({
						active: true,
						queryOptions: {sort: {'position': 1}}
					}, function (data) {
						self.data = data;
						self.render(data);

						self.initChangeOrientationEvent();
					});
				},

				render: function (data) {
					var self = this;

					self.element.html(can.view(self.options.viewpath + 'index' + appState.attr('deviceViewport') + '.stache', {
							components: data,
							appState: appState
						}, {

						})
					);

					if (data.length >= 16) {
						var i = 16;
						while (i < data.length) {
							self.element.find('.productCategories').append(can.view(self.options.viewpath + 'index_additional' + appState.attr('deviceViewport') + '.stache', {
									components: data.slice(i, 16+i),
									appState: appState
								}, {

								})
							);

							i += 16;
						}
					}

					if (self.options.isReady) {
						self.options.isReady.resolve();
					}
				},

				initChangeOrientationEvent: function () {
					var self = this;

					$(window).on("orientationchange",function(){
						setTimeout(function() {
							self.render(self.data);
						},200);
					});
				}
			});

		}
	);

})();