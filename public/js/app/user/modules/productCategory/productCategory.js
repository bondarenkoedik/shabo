'use strict';

(function(){

	var component = 'productCategory',
		adminComponent = 'productCategories';

	define(
		[
			'canjs',
			'modules/'+adminComponent+'/'+adminComponent+'Model',
			'core/appState',
			'css!app/modules/'+component+'/css/'+component+'.css'
		],

		function (can, ComponentModel, appState) {

			return can.Control.extend({
				defaults: {
					viewpath: 'app/modules/'+component+'/views/'
				}
			}, {
				init: function () {
					var self = this,
						options = self.options,
						link = can.route.attr('id');

					ComponentModel.findBy({
						queryOptions: {sort: {'position': 1}},
						link: link,
						url: '/' + adminComponent + '/' + link
					}, function (data) {
						self.data = data;
						self.render(data);
					});
				},

				render: function (data) {
					var self = this;

					if (data && data.data) {

						self.element.html(can.view(self.options.viewpath + (data.data.view ? data.data.view : 'index') + '.stache', {
								component: data.data,
								appState: appState
							}, {

							})
						);
					}

					if (self.options.isReady) {
						self.options.isReady.resolve();
					}
				},

				'.productItem.module click': function (el, ev) {
					setTimeout(function() {
						appState.attr('productSlide', el.data('index'));
					}, 0);
				}
			});

		}
	);

})();