'use strict';

(function(){

	var component = 'articles',
		related = 'articleCategory';

	define(
		[
			'canjs',
			'modules/'+component+'/'+component+'Model',
			'core/appState',
			'modules/authors/authorsModel',
			'userLib/commentaries/commentaries',
			'bx-slider/jquery.bxslider.min',
			'css!bx-slider/jquery.bxslider.css',
			'css!app/modules/'+component+'/css/'+component+'.css'
		],

		function (can, ComponentModel, appState, AuthorsModel, Commentaries) {

			return can.Control.extend({
				defaults: {
					viewpath: 'app/modules/'+component+'/views/'
				}
			}, {
				init: function () {
					var self = this,
						options = self.options,
						link = can.route.attr('id');

					self.link = link;
					self.articles = null;

					var queryOptions = {sort: {'position': 1}};
					if (link == 'news') {
						queryOptions= {sort: {'sortDate': -1}, fields: {'lang.content': 0}}
					}

					ComponentModel.findAll({
						related: {model: 'ArticleCategory', field: 'articleCategory', link: link},
						excludeRelatedEntities: true,
						queryOptions: queryOptions,
						populateOnly: true
					}, function (data) {
						self.data = data;
						self.render(data);
						if (link == 'advices') {
							self.carousel();
						}
					});
				},

				render: function (data) {
					var self = this;
					var itemsToShow = 3;

					if (!data) location.reload();

/*					self.articles = _.sortBy(data, 'publicationDate').reverse();*/
					self.articles = data;

					if (self.link == 'expert-columns') {
						var paginatedData = self.articles.slice(0, itemsToShow);

						AuthorsModel.findAll({}, function (data) {
							self.renderAuthors(data);
						});
					} else if (self.link == 'news') {
						itemsToShow = 6;
						var paginatedData = self.articles.slice(0, itemsToShow);
					}

					var dataToRender = paginatedData ? paginatedData : self.articles;

					if (data.length > 0) {
						self.element.html(can.view(self.options.viewpath + (self.link ? self.link : 'index') + '.stache', {
								components: dataToRender,
								appState: appState,
								displayMoreExpertArticles: self.articles.length > itemsToShow
							}, {

							})
						);
					}

					if (self.options.isReady) {
						self.options.isReady.resolve();
					}
				},

				carousel: function () {
					var self = this;

					$('.advices', self.element).bxSlider();
				},

				renderAuthors: function (data) {
					var self = this;

					if (data.length > 0) {
						$('.expertsListWrap', self.element).html(can.view(self.options.viewpath + 'expertsList.stache', {
								components: data,
								appState: appState
							}, {

							})
						);
					}

				},

				'.moreExpertsColumns click': function (el, ev) {
					var self = this;

					$('.expertColumnsList', self.element).html(can.view(self.options.viewpath + 'expertColumnList.stache', {
							components: self.articles,
							appState: appState,
							displayMoreExpertArticles: false
						}, {

						})
					);

					el.hide();
				},

				'.moreNews click': function (el, ev) {
					var self = this;

					$('.newsList', self.element).html(can.view(self.options.viewpath + 'newsList.stache', {
							components: self.articles,
							appState: appState,
							displayMoreExpertArticles: false
						}, {

						})
					);

					el.hide();
				},

				'.closeAskExpert click': function (el, ev) {
					$('.askExpertWrapper').velocity('fadeOut');
				},

				'.askExpertBtn click': function (el, ev) {
					if (appState.attr('user')) {
						$('.askExpertErrorWrap').css('display', 'none');
						$('.askExpertFormWrap').velocity('fadeIn');
						$('.askExpertWrapper').velocity('fadeIn');
					} else {
						$('.showRegistration').trigger('click');
					}
				},

				'.askExpertForm submit': function (el, ev) {
					ev.preventDefault();
					var self = this;

					Commentaries.askExpert(el, can.proxy(self.askExpertCallback, self));
					$('.askExpertFormWrap').velocity('fadeOut');
				},

				askExpertCallback: function (data) {
					var self = this;
					var responseContent = appState.attr('locale.askExpertSuccess');
					var $errorWrap = $('.askExpertErrorWrap');

					if (data.errors && data.errors.length > 0) {
						responseContent = data.errors[0].response;
					}
					$errorWrap.find('.content').html(responseContent);
					$errorWrap.velocity('fadeIn');
				},

				'.askExpertErrorWrap .popupReady click': function (el, ev) {
					$('.askExpertWrapper').velocity('fadeOut');
				}
			});

		}
	);

})();