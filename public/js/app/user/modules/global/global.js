define([
        'canjs',
        'core/appState'
        //'social/vk/vk_sdk',
        //'social/fb/fb_sdk'
    ],
    function (can, appState) {

        return can.Control.extend({
            defaults: {
                viewpath: 'app/global/'
            }
        }, {

            init: function () {
                var self = this;

                //self.initVkSDK();
                //self.initFbSDK();
                self.setViewport();
                self.initChangeOrientationEvent();

                if (appState.attr('user')) {
                    $('.showRegistrationPopup').css('display', 'none');
                    $('.logoutButton').css('display', 'inline');
                }
            },

            initVkSDK: function () {
                VK.init({
                    apiId: appState.attr('socialConfig.vk.apiId')
                })
            },

            initFbSDK: function () {

                FB.init({
                    appId: appState.attr('socialConfig.facebook.clientID'),
                    cookie: true,
                    xfbml: true,
                    version: 'v2.1'
                });
            },

            setViewport: function () {
                var windowWidth = $(window).width();

                if (windowWidth > 1024) {
                    appState.attr('deviceViewport', '');
                } else if (windowWidth >= 768) {
                    appState.attr('deviceViewport', '_tablet');
                } else {
                    appState.attr('deviceViewport', '_mobile');
                }
            },

            initChangeOrientationEvent: function () {
                var self = this;

                $(window).on("orientationchange",function(){
                    setTimeout(function () {
                        self.setViewport();
                    }, 150);
                });
            }
        });
    }
);