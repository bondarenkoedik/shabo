'use strict';

function updateQueryStringParameter(uri, key, value) {
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

(function(){

	var component = 'products';

	define(
		[
			'canjs',
			'modules/'+component+'/'+component+'Model',
			'core/appState',
			'bx-slider/jquery.bxslider.min',
			'css!bx-slider/jquery.bxslider.css',
			'css!app/modules/'+component+'/css/'+component+'.css'
		],

		function (can, ComponentModel, appState, bxSlider) {

			return can.Control.extend({
				defaults: {
					viewpath: 'app/modules/'+component+'/views/'
				}
			}, {
				init: function () {
					var self = this,
						options = self.options,
						link = can.route.attr('id');
					self.components = null;
					self.slider = null;

					ComponentModel.findAll({
						queryOptions: {sort: {'position': 1}},
						excludeRelatedEntities: true,
						related: {model: 'ProductCategory', field: 'productCategory', link: link}
					}, function (data) {
						self.data = data;
						self.render(data);
						self.initBindings();
						self.initChangeOrientationEvent();
					});
				},

				render: function (data) {
					var self = this;

					self.components = data;

					var viewFile = self.options.viewpath + 'index' + appState.attr('deviceViewport') + '.stache';

					self.element.html(can.view(viewFile, {
							components: self.components,
							appState: appState
						}, {

						})
					);

					if (self.options.isReady) {
						self.options.isReady.resolve();
						self.carousel();
						self.fillSvg();
						self.iphoneFixes();
					}
				},

				initChangeOrientationEvent: function () {
					var self = this;

					$(window).on("orientationchange",function(){
						setTimeout(function() {
							self.render(self.data);
						},200);
					});
				},

				carousel: function () {
					var self = this;
					var startSlide = 0;

					var slideWidth = 0;
					if ($(window).width() >= 1024) {
						slideWidth = 1024
					}

					if (appState.attr('productSlide')) {
						startSlide = appState.attr('productSlide');
						can.route.attr('product', appState.attr('productSlide'));
					} else if (can.route.attr('product')) {
						startSlide = can.route.attr('product');
					}

					console.log(startSlide);

					self.slider = $('.products', self.element).bxSlider({
						preloadImages: 'visible',
						startSlide: +startSlide,
						minSlides: 1,
						maxSlides: 1,
						slideWidth: slideWidth,
						onSlideAfter: function (_1, _2, product) {
							can.route.attr('product', product)
						},
						onSliderLoad: function () {
							$('.products').removeClass('blurred');
						}
					});
				},

				initBindings: function () {
					var self = this;

					appState.bind('productSlide', function () {
						self.render(self.data);
						//self.slider.goToSlide(+appState.attr('productSlide'));
					});
				},

				iphoneFixes: function () {
					if (navigator.platform.indexOf("iPhone") != -1) {
						$('.recommendationsContent').css('font-size', '8px');
						setTimeout(function () {
							$('.recommendationsContent').css('font-size', '8px');
						}, 300);
					}
				},

				fillSvg: function () {
					$('object').on('load', function () {
						var $svgDoc = $(this.contentDocument);
						var color = $(this).data('color') || '#ffffff';
						$svgDoc.find('path').css({'fill': color});
						$svgDoc.find('polygon').css({'fill': color});
						$svgDoc.find('rect').css({'fill': color});
					});
				}
			});

		}
	);

})();