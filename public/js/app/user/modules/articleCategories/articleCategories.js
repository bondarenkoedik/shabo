'use strict';

(function(){

	var component = 'articleCategories';

	define(
		[
			'canjs',
			'modules/'+component+'/'+component+'Model',
			'core/appState',
			'css!app/modules/'+component+'/css/'+component+'.css'
		],

		function (can, ComponentModel, appState) {

			return can.Control.extend({
				defaults: {
					viewpath: 'app/modules/'+component+'/views/'
				}
			}, {
				init: function () {
					var self = this,
						options = self.options;

					ComponentModel.findAll({}, function (data) {
						self.data = data;
						self.render(data);
					});
				},

				render: function (data) {
					var self = this;

					self.element.html(can.view(options.viewpath + 'index.stache', {
							components: data,
							appState: appState
						}, {

						})
					);

					if (options.isReady) {
						options.isReady.resolve();
					}
				}
			});

		}
	);

})();