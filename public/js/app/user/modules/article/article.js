'use strict';

(function(){

	var component = 'article',
		componentPlural = 'articles';

	define(
		[
			'canjs',
				'modules/'+componentPlural+'/'+componentPlural+'Model',
			'core/appState',
			'userLib/commentaries/commentaries',
				'css!app/modules/'+component+'/css/'+component+'.css'
		],

		function (can, ComponentModel, appState, Commentaries) {

			return can.Control.extend({
				defaults: {
					viewpath: 'app/modules/'+component+'/views/'
				}
			}, {
				init: function () {
					var self = this,
						options = self.options,
						link = can.route.attr('id');

					this._id = null;

					ComponentModel.findBy({
						queryOptions: {sort: {'position': 1}},
						link: link,
						url: '/' + component + '/' + link
					}, function (data) {
						self.data = data;
						self.render(data);
					});
				},

				render: function (data) {
					var self = this;

					if (data && data.data) {
						self._id = data.data._id;

						self.element.html(can.view(self.options.viewpath + (data.data.view ? data.data.view : 'index') + '.stache', {
								component: data.data,
								appState: appState
							}, {

							})
						);

						Commentaries.get('article', data.data._id, can.proxy(self.renderCommentaries, self));
					}

					if (self.options.isReady) {
						self.options.isReady.resolve();
					}
				},

				renderCommentaries: function (commentaries) {
					var self = this;

					if (!commentaries) return false;

					$('.commentariesWrapper', self.element).html(can.view(self.options.viewpath + 'commentaries.stache', {
							commentaries: commentaries,
							appState: appState
						}, {

						})
					);

					$('.commentsAmount', self.element).html(commentaries.length);
				},

				'.addCommentary submit': function (el, ev) {
					ev.preventDefault();
					var self = this;

					Commentaries.add('article', self._id, el, can.proxy(self.renderNewCommentary, self));
					el.find('textarea').val('');
				},

				renderNewCommentary: function (data) {
					var self = this;

					if (data) {

						$('.commentariesWrapper', self.element).append(can.view(self.options.viewpath + 'commentaryItem.stache', {
								user: appState.attr('user'),
								date: data.date,
								content: data.content,

								appState: appState
							}, {

							})
						);
					}
				},

				'.askExpertBtn click': function (el, ev) {
					if (appState.attr('user')) {
						$('.askExpertErrorWrap').css('display', 'none');
						$('.askExpertFormWrap').velocity('fadeIn');
						$('.askExpertWrapper').velocity('fadeIn');
					} else {
						$('.showRegistration').trigger('click');
					}
				},

				'.askExpertForm submit': function (el, ev) {
					ev.preventDefault();
					var self = this;

					Commentaries.askExpert(el, can.proxy(self.askExpertCallback, self));
					$('.askExpertFormWrap').velocity('fadeOut');
				},

				askExpertCallback: function (data) {
					var self = this;
					var responseContent = appState.attr('locale.askExpertSuccess');
					var $errorWrap = $('.askExpertErrorWrap');

					if (data.errors && data.errors.length > 0) {
						responseContent = data.errors[0].response;
					}
					$errorWrap.find('.content').html(responseContent);
					$errorWrap.velocity('fadeIn');
				},

				'.askExpertErrorWrap .popupReady click': function (el, ev) {
					$('.askExpertWrapper').velocity('fadeOut');
				},

				'.closeAskExpert click': function (el, ev) {
					$('.askExpertWrapper').velocity('fadeOut');
				}

			});

		}
	);

})();