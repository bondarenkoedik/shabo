'use strict';

(function(){

	var component = 'feedback';

	define(
		[
			'canjs',
			'modules/'+component+'/'+component+'Model',
			'core/appState',
			'jquery-form/jquery.form',
			'css!app/modules/'+component+'/css/'+component+'.css'
		],

		function (can, ComponentModel, appState) {

			return can.Control.extend({
				defaults: {
					viewpath: 'app/modules/'+component+'/views/'
				}
			}, {
				init: function () {
					var self = this,
						options = self.options,
						link = can.route.attr('id');
					self.feedback = null;

					ComponentModel.findAll({queryOptions: {sort: {'date': -1}}}, function (data) {
						self.data = data;
						self.render(data);
					});
				},

				render: function (data) {
					var self = this;

					self.feedback = data;
					var paginatedData = self.feedback.slice(0, 5);

					self.element.html(can.view(self.options.viewpath + 'index.stache', {
							components: paginatedData,
							appState: appState,
							feedbackItemViewpath: self.options.viewpath + 'feedbackItem.stache',
							displayMoreFeedbackBtn: data.length > 5
						}, {

						})
					);

					if (self.options.isReady) {
						self.options.isReady.resolve();
					}
				},

				'.sendFeedback submit': function (el, ev) {
					ev.preventDefault();

					var self = this,
						form = el.ajaxSubmit({
							method: 'POST',
							url: '/feedback'
						});

					form.data('jqxhr').done(function (data) {
						self.renderResponse(data);
					}).fail(function (data) {
						console.error(data);
					});
				},

				renderResponse: function (data) {
					var self = this;

					if (data) {

						$('.feedbackList', self.element).prepend(can.view(self.options.viewpath + 'feedbackItem.stache', {
								user: appState.attr('user'),
								date: data.data.date,
								content: data.data.content,

								appState: appState
							}, {

							})
						);
					}
				},

				'.moreFeedback click': function (el, ev) {
					var self = this;
					el.hide();

					var paginatedData = self.feedback.slice(5, self.feedback.length);

					_.each(paginatedData, function(element) {
						$('.feedbackList', self.element).append(can.view(self.options.viewpath + 'feedbackItem.stache', {
								user: element.user,
								date: element.date,
								content: element.content,
								appState: appState
							}, {

							})
						);
					});
				}
			});

		}
	);

})();