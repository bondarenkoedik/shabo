'use strict';

(function(){

	var component = 'contacts';

	define([
			'canjs',
			'modules/'+component+'/'+component+'Model',
			'core/appState',
			'velocity',
			'css!app/modules/'+component+'/css/'+component+'.css'
		],

		function (can, ComponentModel, appState) {

			return can.Control.extend({
				defaults: {
					viewpath: 'app/modules/'+component+'/views/'
				}
			}, {
				init: function () {
					var self = this;
					self.components = null;

					ComponentModel.findAll({}, function (data) {
						self.data = data;
						self.render(data);

						self.initChangeOrientationEvent();
					});
				},

				'{window} resize': function() {
					this.render(this.data);
				},

				render: function (data) {
					var self = this;

					self.components = data;

					self.element.html(can.view(self.options.viewpath + 'index' + appState.attr('deviceViewport') + '.stache', {
							components: data,
							appState: appState
						}, {

						})
					);

					if (self.options.isReady) {
						self.options.isReady.resolve();

						if (self.components && self.components.length > 0) {
							self.initMap();
						}
					}
				},

				initChangeOrientationEvent: function () {
					var self = this;

					$(window).on("orientationchange",function(){
						setTimeout(function() {
							self.render(self.data);
						},200);
					});
				},

				initMap: function () {
					var self = this;

					var $mapWrapper = $('#contactsMap', self.element);
					var $firstContact = $('.contactsItem', self.element).first();

					$firstContact.addClass('active');
					$mapWrapper.html(self.components[0].map);
				},

				'.contactsItem click': function (el, ev) {
					var self = this;
					var index = parseInt(el.data('index'));

					if (!el.hasClass('active')) {

						var $mapWrapper = $('#contactsMap');

						$('.contactsItem.active').removeClass('active');
						el.addClass('active');

						$mapWrapper.velocity('fadeOut', function() {
							$mapWrapper.html(self.components[index].map);
							$mapWrapper.velocity('fadeIn');
						});
					}
				}
			});

		}
	);

})();