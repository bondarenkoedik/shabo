'use strict';

(function(){

	var component = 'faqs';

	define([
			'canjs',
			'modules/'+component+'/'+component+'Model',
			'core/appState',
			'velocity',
			'css!app/modules/'+component+'/css/'+component+'.css'
		],

		function (can, ComponentModel, appState) {

			return can.Control.extend({
				defaults: {
					viewpath: 'app/modules/'+component+'/views/',
					slice: 10
				}
			}, {
				init: function () {
					var self = this;

					ComponentModel.findAll({}, function (data) {
						self.data = data;
						self.render(data);
					});
				},

				render: function (data) {
					var self = this;

					self.faqs = data;
					var paginatedData = self.faqs.slice(0, self.options.slice);

					self.element.html(can.view(self.options.viewpath + 'index.stache', {
							components: paginatedData,
							appState: appState,
							displayMoreBtn: self.faqs.length > self.options.slice
						}, {

						})
					);

					if (self.options.isReady) {
						self.options.isReady.resolve();
					}
				},

				'.toggleCollapse click': function (el, ev) {
					var _id = el.data('href');
					var $content = $('#'+_id);

					if ($content.hasClass('visible')) {
						$content.velocity('slideUp');
						el.removeClass('visible');
						$content.removeClass('visible');
					} else {
						$content.velocity('slideDown');
						el.addClass('visible');
						$content.addClass('visible');
					}
				},

				'.loadMoreFaqs click': function (el, ev) {
					var self = this;
					el.hide();

					var paginatedData = self.faqs.slice(self.options.slice, self.faqs.length);

					_.each(paginatedData, function(element) {
						$('.faqsList', self.element).append(can.view(self.options.viewpath + 'faq.stache', {
								lang: element.lang,
								appState: appState
							}, {

							})
						);
					});
				}

			});

		}
	);

})();